package ru.kharlamova.tm.api.entity;

import ru.kharlamova.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
