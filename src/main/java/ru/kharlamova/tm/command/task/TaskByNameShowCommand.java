package ru.kharlamova.tm.command.task;

import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.model.Task;
import ru.kharlamova.tm.util.TerminalUtil;

public class TaskByNameShowCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-view-by-name";
    }

    @Override
    public String description() {
        return "Show a task by name.";
    }

    @Override
    public void execute() {
        System.out.println("[SHOW TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        showTask(task);
    }

}
