package ru.kharlamova.tm.command.task;

import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.model.Task;
import ru.kharlamova.tm.util.TerminalUtil;

public class TaskByNameFinishCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-name";
    }

    @Override
    public String description() {
        return "Change task status to Complete by task name.";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER NAME:]");
        final String name = TerminalUtil.nextLine();
        final Task task = serviceLocator.getTaskService().finishTaskByName(name);
        if (task == null) throw new TaskNotFoundException();
    }

}
