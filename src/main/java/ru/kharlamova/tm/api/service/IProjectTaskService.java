package ru.kharlamova.tm.api.service;

import ru.kharlamova.tm.model.Project;
import ru.kharlamova.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String projectId);

    Task bindTaskByProject(String taskId, String projectId);

    Task unbindTaskFromProject(String taskId);

    Project removeProjectById(String projectId);

}
