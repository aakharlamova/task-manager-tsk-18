package ru.kharlamova.tm.exception.authorization;

import ru.kharlamova.tm.exception.AbstractException;

public class LoginExistException extends AbstractException {

    public LoginExistException() {
        super("Error! Login already exists. Please try again.");
    }

}
