package ru.kharlamova.tm.command.task;

import ru.kharlamova.tm.command.AbstractTaskCommand;
import ru.kharlamova.tm.exception.entity.TaskNotFoundException;
import ru.kharlamova.tm.model.Task;
import ru.kharlamova.tm.util.TerminalUtil;

public class TaskByIndexFinishCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-finish-by-index";
    }

    @Override
    public String description() {
        return "Change task status to Complete by task index.";
    }

    @Override
    public void execute() {
        System.out.println("[FINISH TASK]");
        System.out.println("[ENTER INDEX:]");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = serviceLocator.getTaskService().finishTaskByIndex(index);
        if (task == null) throw new TaskNotFoundException();
    }

}
