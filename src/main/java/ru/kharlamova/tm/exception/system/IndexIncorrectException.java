package ru.kharlamova.tm.exception.system;

import ru.kharlamova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(final String value) {
        super("Error! This value ``" +  value + "`` is not number.");
    }

    public IndexIncorrectException() {
        super("Error! This index is incorrect.");
    }

}
