package ru.kharlamova.tm.api.service;

import javax.security.auth.callback.PasswordCallback;

public interface ServiceLocator {

    ITaskService getTaskService();

    IProjectService getProjectService();

    ICommandService getCommandService();

    IProjectTaskService getProjectTaskService();

    IAuthService getAuthService();

    IUserService getUserService();

}
