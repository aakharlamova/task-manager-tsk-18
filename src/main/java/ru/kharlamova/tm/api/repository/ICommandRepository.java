package ru.kharlamova.tm.api.repository;

import ru.kharlamova.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandRepository {

    Collection<AbstractCommand> getArguments();

    Collection<AbstractCommand> getCommands();

    Collection<String> getCommandArgs();

    Collection<String> getCommandNames();

    AbstractCommand getCommandByArg(String arg);

    AbstractCommand getCommandByName(String name);

    void add(AbstractCommand command);

}
